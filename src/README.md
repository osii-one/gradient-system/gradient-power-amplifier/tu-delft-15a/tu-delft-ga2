# Source README

The PCB source files have been created with [EAGLE](https://en.wikipedia.org/wiki/EAGLE_(program)) (proprietary).
The panel design of the enclosure has been done with [Front Panel Designer](https://www.frontpanelexpress.com/fpd-doc/en/index.htm?fpd_welcome_frontpld.htm) (freeeware).
For export formats, alongside with an extensive document on design notes, please refer to the respective [release notes](https://gitlab.com/osii/gradient-system/gradient-power-amplifier/tu-delft-ga2/-/releases).

