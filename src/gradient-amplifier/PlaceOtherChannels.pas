Var
    Board         : IPCB_Board;
    Comp2,Comp1   : IPCB_Component;
    n             : Integer;
    i             : Integer;
    nparts        : array[0..10] of Integer;
    partType      : array[0..10] of Char;
    component     : char;
    ToChannel     : Integer;
    pitch         : float;

Begin
    // Retrieve the current board
    Board := PCBServer.GetCurrentPCBBoard;
    If Board = Nil Then
    begin
         ShowMessage('Execute from PCB document');
         Exit;
    end;
    // Find similar components and place them on a next channel
    partType := ['R','C','Q','U','D','P','J','L']; // component type to process
    nparts := [71, 33, 25, 9, 8, 3, 3, 1];    // number of parts per channel
    ToChannel := 2; // channel to place at
    pitch := 60;    // channel spacing in [mm]
    PCBServer.PreProcess;
    while toChannel <= 3 do

    begin
        while i < 8 do
        begin
            n := 1;
            while(n <= nparts[i] ) do
             begin
                 Comp1 := Board.GetPcbComponentByRefDes(partType[i] + IntToStr(n));
                 if Comp1 = Nil then ShowMessage(partType[i] + IntToStr(n) + ' not found');
                 Comp2 := Board.GetPcbComponentByRefDes(partType[i] + IntToStr(n+(nparts[i]*(ToChannel-1))));
                 if Comp2 = Nil then ShowMessage(partType[i] + IntToStr(n) + ' not found for next channel');
                 PCBServer.SendMessageToRobots(Comp2.I_ObjectAddress, c_Broadcast, PCBM_BeginModify , c_NoEventData);
                 Comp2.Layer := Comp1.Layer;
                 Comp2.x := Comp1.x - MMsToRealCoord(pitch * (ToChannel-1));
                 Comp2.y := Comp1.y;
                 Comp2.Rotation := Comp1.Rotation;
                 PCBServer.SendMessageToRobots(Comp2.I_ObjectAddress, c_Broadcast, PCBM_EndModify , c_NoEventData);
                 n := n+1;
             end;
            i := i+1;
        end;
        toChannel := toChannel + 1;
        i := 0;
    end;
    Client.SendMessage('PCB:Zoom', 'Action=Redraw', 255, Client.CurrentView);     // alleen de laatste keer uitvoeren
    PCBServer.PostProcess;
    ShowMessage('Save PCB and close before running this script again');
End
